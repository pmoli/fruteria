import React, { useState } from 'react';

export default (props) => {
    const Contenedor = {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        padding: '5px',
        height: '60px',
        width: '200px',
        color: 'white',
        backgroundColor: 'darkgreen',
        margin: '0 0 5px 5px',
    }

    const array = props.contVenta.map(el => {
        if (el.units > 0) {
            return (
                <div key={el.id} style={Contenedor}>
                    {el.nom}
                    <br/>
                    {el.preu}€ x {el.units} = {el.units*el.preu}€

                </div>
            )
        }
    }
    )
    return (
        <>
            {array}
        </>
    )
}