export const productos = [
    {
        "id" : 1,
        "nom" : "Plàtan",
        "preu" : 0.5,
        "units" : 0
    },
    {
        "id" : 2,
        "nom" : "Poma",
        "preu": 0.8,
        "units" : 0
    },
    {
        "id" : 3,
        "nom" : "Pinya",
        "preu": 5,
        "units" : 0
    },
    {
        "id" : 4,
        "nom" : "Meló",
        "preu": 6,
        "units" : 0
    },
];
