import React from 'react';
import Total from './Total';
import Venta from './Venta';

export default (props) => {

    return (
        <div>
            <Venta contVenta={props.contVenta} />
            <Total />
        </div>
    )
}