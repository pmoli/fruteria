import React from 'react';

export default ()=>{
    const totalContenedor = {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: '5px',
        height: '50px',
        width: '200px',
        border:'1px black solid',
        margin: '0 0 5px 5px',
    }
    return(
    <div style={totalContenedor}>
        Total: 
    </div>
    )
}