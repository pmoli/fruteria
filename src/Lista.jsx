import React from 'react';

import Afegir from './Afegir';


export default (props) => {
    const Contenedor = {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        padding: '5px',
        height: '50px',
        width: '200px',
        color: 'white',
        backgroundColor: 'green',
        marginBottom: '5px',
    }
    const listaNombres = props.producto.map(el => (
        <div key={el.id} style={Contenedor}>
            {el.nom} ({el.preu} €/u)
            <Afegir id={el.id} clica={props.clic} />
        </div>
    ))

    return (
        <>
            <div>
                {listaNombres}
            </div>
        </>
    )
}